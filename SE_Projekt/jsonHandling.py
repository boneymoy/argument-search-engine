import json
import csv

def jprint(obj):
    # create formatted string of the python JSON object
    text = json.dumps(obj, sort_keys=True, indent=4)
    print(text)

def saveJSONFile(data, path):
    print(data["arguments"][0])
    with open(path, 'w') as f_out:
        for i in range(len(data["arguments"])):
            json.dump(data["arguments"][i], f_out)
            
def saveCSVFile(list_of_dict, path):
    with open(path, 'w') as f:
        for i in range(len(list_of_dict)):
            for key in list_of_dict[i].keys():
                f.write("%s,%s\n"%(key,list_of_dict[i][key]))
                
def readCSVFile(path):
    with open(path, newline='') as csv_file:
        reader = csv.reader(csv_file, delimiter=' ', quotechar='|')
        for row in reader:
            print(', '.join(row))
            break
    return list_of_dict
def readJSONFile(path):
    #
    # Aufbau JSON File
    # data["arguments"] listet alle Argumente mit allen Metadaten
    # data["arguments"][0] listet das erste Argument der gesamten JSON File  
    # data["arguments"][0]["conclusion"] liefert nur conclusion des ersten Arguments
    #                     ["id"]
    #                     ["premises"][0] gibt erste Begruendung zurueck
    #                     ["premises"][0]["stance"]            
    #                                    ["text"]
    #                     ["context"] liefert gesamten Kontext incl:
    #                     ["context"]["acquisitionTime"]
    #                                ["discussionTitle"]
    #                                ["nextArgumentSourceId"]
    #                                ["previousArgumentInSourceId"]
    #                                ["sourceId"]
    #                                ["sourceTitle"] z.B. "Debate Argument: Contraceptive Forms for High School Students | Debate.org"
    #                                ["sourceUrl"]
    with open(path) as json_file:
        arguments = json.load(json_file)
        
    return arguments
                

