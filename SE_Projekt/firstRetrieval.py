import scipy
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import stopwords
import pandas as pd
import jsonHandling
import k_means_clustering as cluster
from sklearn.metrics.pairwise import linear_kernel
import pickle
import timeit

def predictClusterForQuery(query):
    # Load pretrained Model                    #
    # Predict label of cluster for given query #
    model_path = "kmeans"
    data_path = "Results"
    model, X, vectorizer = cluster.loadModel(model_path)
    results = cluster.loadResults(data_path)
    print("Loaded Model")
    predictedClusterLabel = cluster.KMeansPrediction(query, model, X, vectorizer)
    print("Predicted label ", predictedClusterLabel, " for query: ", query)
    return results, predictedClusterLabel

def rankArguments(data, indices, query):
    # create Tfidf Matrix of test and training set
    # training set is argumentText of each argument in predefined cluster indicated with list of indices)
    # test set is given query
    # TODO: save data of tfidf of all given arguments to reduce runtime

    arguments = pd.DataFrame()

    stop_words = stopwords.words('english')
    vectorizer = TfidfVectorizer(stop_words) 

    testSet = []
    testSet.append(query)

    #create train set with only documents in same cluster
    trainSet = []
    for index in indices:
        trainSet.append(data['arguments'][index]['premises'][0]['text']) 
    trainVectorizer = vectorizer.fit_transform(trainSet) 
    testVectorizer = vectorizer.transform(testSet)
    
    # calculate cosine similarity
    # save argument rank in related_docs_indices
    
    cosine_similarities = linear_kernel(testVectorizer, trainVectorizer).flatten()
    # indices of argument in cluster (not whole document)
    related_docs_indices = np.argsort(cosine_similarities)
    # create list of indices of whole document from list of indices of only cluster
    related_docs_indices = [indices[i] for i in related_docs_indices]

    return related_docs_indices 


def preprocessData(data):
    data['argumentText'] = np.lower(data['argumentText'])
        #remove stop_words?
        #remove singel charakters
        #first lemmatize than
        #stemming
        #Tf-idf
        # consine similarity
def printArguments(data, index_list):
    # data: list of all arguments
    # index_list sorted list of indexes of cosine similarity in ascending order
    counter = 0
    for index in reversed(index_list):
        if len(data['arguments'][index]['premises'][0]['text']) > 400 and counter < 10:
            print(data['arguments'][index]['premises'][0]['text'])
            print(data['arguments'][index]['premises'][0]['stance'])
            print(data['arguments'][index]['context']['sourceTitle'])
            #print(data['arguments'][index]['context']['nextArgumentSourceId'])
            counter += 1
            
def startSearch(query):
    start = timeit.default_timer() 
    filename = 'args-me.json'
    cluster_size = 8000
    #results = cluster.runKMeans(filename, cluster_size)
    results, predicted_cluster_label = predictClusterForQuery(query)
    samples_in_cluster_indices = cluster.argumentsInSameCluster(results, predicted_cluster_label)
    # TODO add previousArguments in runKmeans
    print("Number of samples in cluster: ", len(samples_in_cluster_indices))
    arguments_index = rankArguments(results, samples_in_cluster_indices, query)
    printArguments(results, arguments_index)
    stop = timeit.default_timer()
    print('Time: ', round(stop - start, 2), 's')

if __name__ == '__main__':

    startSearch("abortion ban")
