import jsonHandling
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from sklearn.metrics import adjusted_rand_score
import pandas as pd
import pickle
#
# Go through json and add conclusions to list which is returned
#
# create hashtable/dictionary to filter out ocurrences - if key(conclusion) in hT conclusion already in List
#
def clearUpList(documentList):
    argumentList = []
    for i in range(len(documentList)):
        argumentList.append(documentList[i]["conclusion"])
    return argumentList

def clearUpList_old(documentList, typeOfText):
    if typeOfText == "argument":
        # create list of only argument premisis
        hashTable = {}
        argumentList = []
        for i in range(len(documentList["arguments"])):
            argumentList.append(documentList["arguments"][i]["premises"][0]["text"])
        return argumentList[:1000]
    elif typeOfText == "conclusion":
            # create list of only conclusions
        hashTable = {}
        conclusionList = []
        for i in range(len(documentList["arguments"])):
            conclusionList.append(documentList["arguments"][i]["conclusion"])
        return conclusionList
    else: return None

def trainModel(documents, clusterSize):
    # Convert strings to numbers for KMeans
    # run Tf-idf on documents
    vectorizer = TfidfVectorizer(stop_words='english')
    X = vectorizer.fit_transform(documents)
    #
    #initialize KMeans with clusterSize as number of clusters
    #                       'k-means++' selects inital cluster for kMean not random  
    #
    model = KMeans(n_clusters=clusterSize, init='k-means++', max_iter=100, n_init=1)
    model.fit(X)

    return model, X, vectorizer

def addLablesToDocumentList(labels, documents):
    for i in range(len(documents["arguments"])):
        documents["arguments"][i]["label"] = labels[i]
    return documents



#conclusionList = []
    #argumentTextList = []
    #stanceList = []

    #for i in range(len(documents["arguments"])):
    #    conclusionList.append(documents["arguments"][i]["conclusion"])
    #    argumentTextList.append(documents["arguments"][i]["premises"][0]["text"])
    #    stanceList.append(documents["arguments"][i]["premises"][0]["stance"])

    #print(documents["arguments"][0]["conclusion"])
    #print(documents["arguments"][0]["premises"][0]["text"])
    #print(documents["arguments"][0]["premises"][0]["stance"])
    
    #helpDict = {'conclusion': conclusionList,
    #            'argumentText': argumentTextList,
    #            'stance': stanceList,
    #            'label': labels}
    #for x in helpDict:
    #    print(x)
    #    break
    #TODO add needed information of each argument in this list

def saveModel(model, X, vectorizer, filename):
    pickle.dump(X, open(filename + "X.sav", 'wb'))
    pickle.dump(model, open(filename + "Model.sav", 'wb'))
    pickle.dump(vectorizer, open(filename + "Vec.sav", 'wb'))

def loadModel(filename):
    X = pickle.load(open(filename + "X.sav", 'rb'))
    model = pickle.load(open(filename + "Model.sav", 'rb'))
    vectorizer = pickle.load(open(filename + "Vec.sav", 'rb'))
    return model, X, vectorizer

def KMeansPrediction(query, model, X, vectorizer):
    Y = vectorizer.transform([query])
    prediction = model.predict(Y)
    return prediction[0]

def documentsInSameCluster(results, label):
    labelReached = False
    labelSurpassed = False
    for index, row in results.iterrows():
        if row['label'] == label:
            if not labelReached:
                labelIndex = index - 1
            labelReached = True
            
        elif row['label'] > label:
            if not labelSurpassed:
                results = results[labelIndex:index-1]
            labelSurpassed = True
    
    return results

def argumentsInSameCluster(results, label):
    
    sameClusterIndex = []
    for index in range(len(results["arguments"])):
        if results["arguments"][index]["label"] == label:
            sameClusterIndex.append(index)
        #elif results["arguments"][index]["label"] > label: break
    return sameClusterIndex

def sortResults(results):
    resultList = results["arguments"]
    sortedLabel = sorted(resultList, key=lambda x: x["label"])
    results["arguments"] = sortedLabel
    return results

def runKMeans(filename, cluster_size):
    print("Start kMeans clustering") 
    # Erstellen des KMeans Modelles aufgrund der bereitgestellten Argumente
    originalDocument = jsonHandling.readJSONFile(filename)

    originalDocumentList = originalDocument["arguments"]
    documentList = clearUpList(originalDocumentList)
    print("Start training model")
    model, X, vectorizer = trainModel(documentList, cluster_size)
    print("Finished training model")
    results = addLablesToDocumentList(model.labels_, originalDocument)
    # speichern des Modelles sowie der erstellten gelableten Dokumentenliste
    path = "labeledArguments"

    print("Saving result list and model")
    saveResults(results, "Results")
    saveModel(model, X, vectorizer, "kmeans")
    return results

def saveResults(data, name):
    with open(name + '.pkl', 'wb') as f:
        pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)

def loadResults(name):
    with open(name + '.pkl', 'rb') as f:
        return pickle.load(f)
    
if __name__ == '__main__':

    filename = 'args-me.json'
    clusterSize = 1000
    runKMeans(filename, clustersize) 
    # Prediction aufgrund von geladenen Models 
    model, X, vectorizer = loadModel("kmeans")
    print("Loaded model")
    results = pd.read_csv(path)
    print("Loaded resulting list")
    
    query = "universal healthcare"

    predictedClusterLabel = KMeansPrediction(query, model, X, vectorizer)
    print("Predicted label: ", predictedClusterLabel, " for query: ", query)

    samplesInCluster = conclusionInSameCluster(results, predictedClusterLabel)
    #TODO arguments/documents kmean both possible!!!
    print("Number of samples in same cluster: ", len(samplesInCluster))
    print(samplesInCluster[:10])
    
    
    
